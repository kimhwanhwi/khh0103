#pragma once
class Hero
{
private:
	int _hp;
	int _mp;
	char _name[256];
	int _weapon;	
	
public:
	Hero();
	virtual ~Hero();

	void Die();
	int Attack();
	
};

