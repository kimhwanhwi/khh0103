#pragma once
class Point
{
public:
	Point();
	Point(int x, int y);
	void Show();


	Point operator++();
	Point operator++(int num);


	Point operator+(Point p);
	Point operator-(Point p);



private:
	int _x, _y;
};